﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UpdateService.Models;

namespace UpdateService.Controllers
{
    public class UpdateController : ApiController
    {
        private static ContentRepository repo = ContentRepository.Current;


        // GET: api/Update
        public List<Content> Get(string dir = "D:\\С#\\Workspace\\UpdateService\\UpdateService")
        {
            return LoadFilesFromDicectory(dir);
        }

        // GET: api/Update/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Update

        public void Post(Content[] file)
        {
            System.IO.File.Create(@"D:\С#\Workspace\UpdateService\UpdateService\" + file[0].FileName);
            Content con = new Content();
            con.FileName = file[0].FileName;
            con.FileSize = 0;
            repo.Add(con);
        }

        // PUT: api/Update/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Update/5
        public void Delete(int id)
        {

        }

        [NonAction]
        public List<Content> LoadFilesFromDicectory(string PathToDir)
        {
            repo.GetContent().DirContentList.Clear();
            var dir = new DirectoryInfo(PathToDir);
            //Get file name and extension
            int i = 0;
            foreach (FileInfo file in dir.GetFiles("*.*", SearchOption.TopDirectoryOnly))
            {
                Content Filecontent = new Content();
                Filecontent.FileName = Path.GetFileName(file.FullName);

                Filecontent.FileSize = file.Length;
                // System.Diagnostics.Debug.WriteLine(Filecontent.Id + "  " + Filecontent.FileName + " " + Filecontent.FileSize);
                repo.Add(Filecontent);
                i++;
            }
            int j = 0;
            //Get directory name and extension
            foreach (DirectoryInfo file in dir.GetDirectories("*.*", SearchOption.TopDirectoryOnly))
            {
                Content Dircontent = new Content();
                if (repo.GetSize() != 0)
                    Dircontent.Id = repo.GetAll().Max(t => t.Id) + 1;
                else
                    Dircontent.Id = 0;
                Dircontent.FileName = Path.GetFileName(file.FullName);
                Dircontent.FileSize = GetDirectorySize(Path.GetFullPath(file.FullName));
                repo.Add(Dircontent);
                j++;
            }
            return repo.GetAll();
        }

        public static long GetDirectorySize(string p)
        {
            long folderSize = 0;
            var dir = new DirectoryInfo(p);
            FileInfo[] files = dir.GetFiles("*", SearchOption.AllDirectories);
            foreach (FileInfo file in files) folderSize += file.Length;
            return folderSize;
        }
    }
}
