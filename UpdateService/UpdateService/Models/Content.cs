﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UpdateService.Models
{
    public class Content
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public long FileSize { get; set; }
    }
}