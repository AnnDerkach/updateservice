﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UpdateService.Models
{
    public class ContentRepository
    {

        public static ContentListModel content;
        private static ContentRepository repo = new ContentRepository();

        public static ContentRepository Current
        {
            get
            {
                return repo;
            }
        }

        public ContentListModel GetContent()
        {
            return content;
        }

        public ContentRepository()
        {
            content = new ContentListModel();
        }

        public List<Content> GetAll()
        {

            return content.DirContentList.ToList();
        }


        public int GetSize()
        {
            return content.DirContentList.Count();
        }

        public Content Get(int i)
        {
            return content.DirContentList[i];
        }

        public void Add(Content item)
        {
            if (repo.GetSize() != 0)
                item.Id = repo.GetAll().Max(t => t.Id) + 1;
            else
                item.Id = 0;
            content.DirContentList.Add(item);
        }


        public void Remove(int id)
        {
            Content item = Get(id);
            if (item != null)
            {
                content.DirContentList.Remove(item);
            }

        }

        public void Set(int i,Content item)
        {
           
            if (item != null)
            {
                System.Diagnostics.Debug.WriteLine(i);
              

                content.DirContentList[i].Id = item.Id;
                content.DirContentList[i].FileName = item.FileName;
                content.DirContentList[i].FileSize = item.FileSize;
                System.Diagnostics.Debug.WriteLine(content.DirContentList[i].FileName);
            }

        }

        public bool Update(Content item)
        {
            Content storedItem = Get(item.Id);
            if (storedItem != null)
            {
                storedItem.FileName = item.FileName;
                storedItem.FileSize = item.FileSize;
                return true;
            }
            else
            {
                return false;
            }
        }
    
}
}